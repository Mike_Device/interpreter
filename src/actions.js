'use strict';

var Actions = function() {};

module.exports = Actions;

Actions.prototype.initialize = function() {
	this.str = '';
};

Actions.prototype.getString = function() {
	return this.str;
};

Actions.prototype.plus = function() {
	this.str += '+';
};

Actions.prototype.multiply = function() {
	this.str += '*';
};

Actions.prototype.number = function(number) {
	this.str += number;
};
