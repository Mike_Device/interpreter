'use strict';

var _ = require('underscore'),
	Actions = require('./actions'),
	errors = require('./errors');

var Interpreter = function() {};

module.exports = Interpreter;

Interpreter.prototype.initialize = function(expression) {
	this.expression = expression || '';
	this.index = 0;
	this.currentLexeme = this.getCurrentLexeme();

	this.actions = new Actions();
	this.actions.initialize();
};

Interpreter.prototype.getCurrentLexeme = function() {
	var lexeme = this.expression[this.index],
		number = Number(lexeme);

	return _.isNaN(number) ? lexeme : number;
};

Interpreter.prototype.isEndReached = function() {
	return this.index === this.expression.length;
};

Interpreter.prototype.readLexeme = function() {
	if (!this.isEndReached()) {
		this.index++;
		this.currentLexeme = this.getCurrentLexeme();
		return this.currentLexeme;
	}
};

Interpreter.prototype.throwError = function(type, message) {
	type = type || 0;
	message = message || 'unexpected error';

	throw new errors.BaseError({
		name: type,
		message: message,
		currentLexeme: this.getCurrentLexeme()
	});
};

Interpreter.prototype.E = function() {
	if (this.currentLexeme === '(' || _.isNumber(this.currentLexeme)) {
		this.T(this.currentLexeme);
		this.E_(this.currentLexeme);
	} else {
		this.throwError('E');
	}
};

Interpreter.prototype.T = function() {
	if (this.currentLexeme === '(' || _.isNumber(this.currentLexeme)) {
		this.P(this.currentLexeme);
		this.T_(this.currentLexeme);
	} else {
		this.throwError('T');
	}
};

Interpreter.prototype.P = function() {
	if (this.currentLexeme === '(') {
		this.readLexeme();
		this.E();

		if (this.currentLexeme === ')') {
			this.readLexeme();
		} else {
			this.throwError('P', 'nested condition');
		}
	} else if (_.isNumber(this.currentLexeme)) {
		var lexeme = this.currentLexeme;
		this.readLexeme();
		this.actions.number(lexeme);
	} else {
		this.throwError('P');
	}
};

Interpreter.prototype.E_ = function() {
	if (this.currentLexeme === '+') {
		this.readLexeme();
		this.T();
		this.actions.plus();
		this.E_();
	} else if (this.currentLexeme === ')' || this.isEndReached()) {
		return;
	} else {
		this.throwError('E_');
	}
};

Interpreter.prototype.T_ = function() {
	if (this.currentLexeme === '*') {
		this.readLexeme();
		this.P();
		this.actions.multiply();
		this.T_();
	} else if (
		this.currentLexeme === '+' || this.currentLexeme === ')' ||
		this.isEndReached()
	) {
		return;
	} else {
		this.throwError('T_');
	}
};

Interpreter.prototype.run = function(expression) {
	this.initialize(expression);

	this.E();

	if (!this.isEndReached()) {
		this.throwError('Bad expression');
	}

	return this.actions.getString();
};
