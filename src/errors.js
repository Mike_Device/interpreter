'use strict';

var inherits = require('inherits'),
	_ = require('underscore');

var BaseError = function(params) {
	if (_.isString(params)) {
		params = {message: params};
	}

	var errorFields = ['name', 'message', 'userMessage', 'currentLexeme'];

	_(this).extend(_(params).pick(errorFields));
};

inherits(BaseError, Error);

exports.BaseError = BaseError;
